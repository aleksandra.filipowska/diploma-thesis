FROM maven:3.6.3-jdk-8
WORKDIR /diploma-thesis
ADD /src src
ADD pom.xml .
CMD ["mvn","verify"]
