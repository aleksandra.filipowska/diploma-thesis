package com.diplomaThesis.utils;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.log4testng.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


import static com.diplomaThesis.utils.PracticePageProperties.LOCAL_REMOTE;
import static com.diplomaThesis.utils.PracticePageProperties.*;


@Slf4j
public class DefaultSeleniumWD {

    private Logger logger = Logger.getLogger(this.getClass());
    public WebDriver driver;

    @BeforeClass
    public void setUp_() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
        if (LOCAL_REMOTE.equals("local")) {
            driver = new ChromeDriver();
        } else {
            try {
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setHeadless(true);
                chromeOptions.addArguments("start-maximized");
                driver = new RemoteWebDriver(new URL("http://selenium-hub:4444/wd/hub"), chromeOptions);
            } catch (MalformedURLException ex) {
                logger.error(ex);
                return;
            }
        }
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        goToDefaultPage();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    protected void goToDefaultPage() {
        prepareBrowser();
        driver.get(BASE_URL);
    }

    protected void prepareBrowser() {
        driver.manage().deleteAllCookies();
        driver.manage().window().setSize(new Dimension(1920, 1080));

    }

//    @AfterMethod
//    public void tearDown(Method method) {
//        takeScreenshot(method);
//    }
//
//    @AfterMethod
//    public void takeScreenshot(Method method) {
//        String timeStamp;
//        String screenShotName;
//        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
//        screenShotName = String.format("%s_%s", method.getName(), timeStamp);
//        //screenshot(screenShotName);
//    }

}
