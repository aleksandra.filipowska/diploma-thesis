package com.diplomaThesis.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class SeleniumConfig {

    private SeleniumConfig() {
    }

    public static Properties initConfig(String fileName) {

        Properties defaultConfig = new Properties();
        defaultConfig.setProperty("LOCAL_REMOTE", "remote");
        defaultConfig.setProperty("WD_REMOTE_BROWSER_NAME", "firefox-linux");
        defaultConfig.setProperty("WD_REMOTE_ADRESS_PHP_TRAVELS", "http://selenium-hub:4444/wd/hub");
        defaultConfig.setProperty("BASE_URL", "http://automationpractice.com/index.php");
        defaultConfig.setProperty("SYSTEM_VERSION", System.getProperty("os.name"));

        Properties ret = new Properties(defaultConfig);
        try {
            ret.load(new FileInputStream(fileName));
        } catch (IOException e) {
            log.info("{} not found, see seleniumConfig.cfg.example for reference. Falling back to default config",
                    fileName);
        }
        return ret;
    }
}
