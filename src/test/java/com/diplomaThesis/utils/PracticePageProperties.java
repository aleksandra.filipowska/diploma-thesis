package com.diplomaThesis.utils;

import com.diplomaThesis.utils.SeleniumConfig;
import lombok.extern.slf4j.Slf4j;

import java.util.Properties;

@Slf4j
public class PracticePageProperties {

    private PracticePageProperties() {
    }

    private static final Properties config = SeleniumConfig.initConfig("seleniumConfig.cfg");

    public static final String LOCAL_REMOTE = config.getProperty("LOCAL_REMOTE");
    public static final String BASE_URL = config.getProperty("BASE_URL");
}
