package com.diplomaThesis.tests;

import com.diplomaThesis.pages.DressesSubcategoryPage;
import com.diplomaThesis.pages.FooterComponent;
import com.diplomaThesis.pages.TopsSubcategoryPage;
import com.diplomaThesis.pages.WomanCategoryPage;
import com.diplomaThesis.utils.DefaultSeleniumWD;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class WomanCategoryPageTest extends DefaultSeleniumWD {

    public static final String DRESSES_SUBCATEGORY_NAME = "DRESSES ";
    public static final String TOPS_SUBCATEGORY_NAME = "TOPS ";
    public WomanCategoryPage womanCategoryPage ;
    public FooterComponent footerComponent;
    public TopsSubcategoryPage topsSubcategoryPage;
    public DressesSubcategoryPage dressesSubcategoryPage;
    @BeforeMethod
    public void setUp(){
        footerComponent = new FooterComponent(driver);
        womanCategoryPage = footerComponent.goToWomenCategory();
    }

    @Test
    public void shouldGoToTopsSubcategory() {
        topsSubcategoryPage = womanCategoryPage.goToTopsSubcategory();
        Assert.assertEquals(topsSubcategoryPage.getCategoryName(), TOPS_SUBCATEGORY_NAME);
    }

    @Test(invocationCount = 194)
    public void shouldGoToDressesSubcategory() {
        dressesSubcategoryPage = womanCategoryPage.goToDressesSubcategory();
        Assert.assertEquals(dressesSubcategoryPage.getCategoryName(), DRESSES_SUBCATEGORY_NAME);
    }
}
