package com.diplomaThesis.tests;

import com.diplomaThesis.pages.FooterComponent;
import com.diplomaThesis.pages.HomePage;
import com.diplomaThesis.utils.DefaultSeleniumWD;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FooterComponentTest extends DefaultSeleniumWD {

    public static final String NEWSLETTER_EMAIL = "test@example.com";
    public static final String NEWSLETTER_ALREADY_REGISTERED_ALERT = "Newsletter : This email address is already registered.";
    public FooterComponent footerComponent;
    public HomePage homePage;

    @BeforeMethod
    public void setUp() {
        footerComponent = new FooterComponent(driver);
    }

    @Test
    public void shouldFailToSubscribeToNewsletter() {
        homePage = footerComponent.subscribeToNewsletter(NEWSLETTER_EMAIL);
        Assert.assertEquals(homePage.getNewsletterAlert(), NEWSLETTER_ALREADY_REGISTERED_ALERT);
    }
}
