package com.diplomaThesis.tests;

import com.diplomaThesis.pages.ContactUsPage;
import com.diplomaThesis.pages.HeaderComponent;
import com.diplomaThesis.pages.SignInPage;
import com.diplomaThesis.utils.DefaultSeleniumWD;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HeaderComponentTest extends DefaultSeleniumWD {

    public static final String CONTACT_US_PAGE_HEADER = "CUSTOMER SERVICE - CONTACT US";
    public static final String INCORRECT_LOGIN = "test@example.com";
    public static final String INCORRECT_PASSWORD = "test";
    public static final String ERRORS_COUNT_TEXT = "There is 1 error";
    public static final String FIRST_ERROR_TEXT = "Invalid password.";
    public SignInPage signInPage;
    public ContactUsPage contactUsPage;
    public HeaderComponent headerComponent;
    @BeforeMethod
    public void setUp(){
        headerComponent = new HeaderComponent(driver);
    }

    @Test
    public void shouldGoToContactUsPage(){
        contactUsPage = headerComponent.goToContactUsPage();
        Assert.assertEquals(contactUsPage.getContactUsPageHeader(), CONTACT_US_PAGE_HEADER);
    }

    @Test
    public void shouldFailToSignIn(){
        signInPage = headerComponent.goToSignInPage();
        signInPage.login(INCORRECT_LOGIN, INCORRECT_PASSWORD);
        Assert.assertEquals(signInPage.getNumberOfErrorsText(), ERRORS_COUNT_TEXT);
        Assert.assertEquals(signInPage.getFirstAlertText(), FIRST_ERROR_TEXT);
    }
}
