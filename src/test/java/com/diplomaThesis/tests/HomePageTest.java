package com.diplomaThesis.tests;

import com.diplomaThesis.pages.CartPage;
import com.diplomaThesis.pages.HomePage;
import com.diplomaThesis.pages.ResultPage;
import com.diplomaThesis.utils.DefaultSeleniumWD;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageTest extends DefaultSeleniumWD {

    public final static String EXPECTED_RESULT_TEXT = "7 results have been found.";
    public CartPage cartPage;
    public HomePage homePage;
    public ResultPage resultPage;
    @BeforeMethod
    public void goToHomePageTest(){
        homePage = new HomePage(driver);
        homePage.goToHomePage();
    }

    @Test
    public void shouldCheckIfCartIsEmpty(){
        cartPage = homePage.goToCart();
        Assert.assertTrue(cartPage.isCartEmpty());
    }

    @Test
    public void shouldSearchForAllDresses(){
        resultPage = homePage.searchItem("dress");
        Assert.assertEquals(resultPage.getResultsText(), EXPECTED_RESULT_TEXT);
    }

}
