package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ResultPage extends DefaultPage {

    public static final By NUMBER_OF_RESULTS = By.cssSelector("span[class='heading-counter']");

    public ResultPage(WebDriver driver) {
        super(driver);
    }

    public String getResultsText(){
        return driver.findElement(NUMBER_OF_RESULTS).getText();
    }
}
