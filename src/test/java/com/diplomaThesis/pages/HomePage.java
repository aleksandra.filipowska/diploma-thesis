package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends DefaultPage{

    public static final By SEARCH_INPUT = By.id("search_query_top");
    public static final By SEARCH_BUTTON = By.name("submit_search");
    public static final By CART = By.cssSelector("a[href*='order'] b");
    public static final By NEWSLETTER_ALERT = By.cssSelector("p[class*='alert-danger']");

    public HomePage(WebDriver driver){
        super(driver);
    }

    public ResultPage searchItem(String item){
        driver.findElement(SEARCH_INPUT).sendKeys(item);
        driver.findElement(SEARCH_BUTTON).click();
        return new ResultPage(driver);
    }

    public CartPage goToCart(){
        driver.findElement(CART).click();
        return new CartPage(driver);
    }

    public String getNewsletterAlert(){
        return driver.findElement(NEWSLETTER_ALERT).getText();
    }
}
