package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WomanCategoryPage extends DefaultPage{

    public static final By TOPS_SUBCATEGORY_IMAGE_LINK = By.cssSelector("li div[class='subcategory-image'] a[title*='Tops']");
    public static final By DRESSES_SUBCATEGORY_IMAGE_LINK =By.cssSelector("li:last-of-type a[title='Dresses']");

    public WomanCategoryPage(WebDriver driver) {
        super(driver);
    }

    public TopsSubcategoryPage goToTopsSubcategory(){
        driver.findElement(TOPS_SUBCATEGORY_IMAGE_LINK).click();
        return new TopsSubcategoryPage(driver);
    }

    public DressesSubcategoryPage goToDressesSubcategory(){
        driver.findElement(DRESSES_SUBCATEGORY_IMAGE_LINK).click();
        return new DressesSubcategoryPage(driver);
    }
}
