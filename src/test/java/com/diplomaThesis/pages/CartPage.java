package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage extends DefaultPage {

    public static final By EMPTY_CART_ALERT = By.cssSelector("p[class*='alert']");

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public boolean isCartEmpty(){
        return driver.findElement(EMPTY_CART_ALERT).isDisplayed();
    }
}
