package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TopsSubcategoryPage extends DefaultPage {

    public static final By TOPS_CATEGORY_NAME = By.cssSelector("span[class='cat-name']");
    public TopsSubcategoryPage(WebDriver driver) {
        super(driver);
    }

    public String getCategoryName(){
        return driver.findElement(TOPS_CATEGORY_NAME).getText();
    }
}
