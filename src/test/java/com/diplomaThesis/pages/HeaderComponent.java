package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HeaderComponent extends DefaultPage{

    public static final By SIGN_IN_LINK = By.className("login");
    public static final By CONTACT_US_LINK = By.id("contact-link");

    public HeaderComponent(WebDriver driver){
        super(driver);
    }

    public ContactUsPage goToContactUsPage(){
        driver.findElement(CONTACT_US_LINK).click();
        return new ContactUsPage(driver);
    }

    public SignInPage goToSignInPage(){
        driver.findElement(SIGN_IN_LINK).click();
        return new SignInPage(driver);
    }

}
