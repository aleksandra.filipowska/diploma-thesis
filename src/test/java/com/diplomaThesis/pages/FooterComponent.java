package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FooterComponent extends DefaultPage {

    public static final By NEWSLETTER_INPUT = By.id("newsletter-input");
    public static final By NEWSLETTER_ACCEPT = By.name("submitNewsletter");
    public static final By WOMEN_CATEGORY_LINK = By.cssSelector("li[class*=last] a[href*='id_category=3&controller']");

    public FooterComponent(WebDriver driver) {
        super(driver);
    }

    public HomePage subscribeToNewsletter(String email){
        driver.findElement(NEWSLETTER_INPUT).sendKeys(email);
        driver.findElement(NEWSLETTER_ACCEPT).click();
        return new HomePage(driver);
    }

    public WomanCategoryPage goToWomenCategory(){
        driver.findElement(WOMEN_CATEGORY_LINK).click();
        return new WomanCategoryPage(driver);
    }
}
