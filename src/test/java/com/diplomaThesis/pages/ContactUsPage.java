package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ContactUsPage extends DefaultPage {

    public static final By CONTACT_US_PAGE_HEADER = By.cssSelector("h1[class*='page']");

    public ContactUsPage(WebDriver driver) {
        super(driver);
    }

    public String getContactUsPageHeader() {
        return driver.findElement(CONTACT_US_PAGE_HEADER).getText();
    }
}
