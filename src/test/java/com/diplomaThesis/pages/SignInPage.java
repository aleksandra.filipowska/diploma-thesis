package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignInPage extends DefaultPage {

    public static final By LOGIN_INPUT = By.id("email");
    public static final By PASSWORD_INPUT = By.id("passwd");
    public static final By SIGN_IN_BUTTON = By.id("SubmitLogin");
    public static final By INCORRECT_CREDENTIALS_ALERT_COUNT = By.cssSelector("div[class*='danger'] p");
    public static final By INCORRECT_CREDENTIALS_ALERT_FIRST_TEXT = By.cssSelector("div[class*='danger'] ol li");

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public void login(String login, String password){
        driver.findElement(LOGIN_INPUT).sendKeys(login);
        driver.findElement(PASSWORD_INPUT).sendKeys(password);
        driver.findElement(SIGN_IN_BUTTON).click();
    }

    public String getNumberOfErrorsText(){
        return driver.findElement(INCORRECT_CREDENTIALS_ALERT_COUNT).getText();
    }

    public String getFirstAlertText(){
        return driver.findElement(INCORRECT_CREDENTIALS_ALERT_FIRST_TEXT).getText();
    }
}
