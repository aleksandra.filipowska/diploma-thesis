package com.diplomaThesis.pages;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.remote.ErrorCodes.TIMEOUT;


public abstract class DefaultPage{

    public WebDriver driver;
    public WebDriverWait driverWait;
    public static final By HOME_PAGE = By.cssSelector("img[class*='logo']");

    public DefaultPage(WebDriver driver) {
        this.driver = driver;
        this.driverWait = new WebDriverWait(driver, 20);
    }

    public HomePage goToHomePage(){
        driver.findElement(HOME_PAGE).click();
        return new HomePage(driver);
    }

}