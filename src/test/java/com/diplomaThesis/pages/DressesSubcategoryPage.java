package com.diplomaThesis.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DressesSubcategoryPage extends DefaultPage {

    public static final By DRESSES_CATEGORY_NAME = By.cssSelector("span[class='cat-name']");
    public DressesSubcategoryPage(WebDriver driver) {
        super(driver);
    }

    public String getCategoryName(){
        return driver.findElement(DRESSES_CATEGORY_NAME).getText();
    }
}
