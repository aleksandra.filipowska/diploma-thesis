\contentsline {chapter}{\numberline {1}Słownik wyrażeń}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Wyjaśnienie terminów występujących w poniższej pracy inżynierskiej}{3}{section.1.1}%
\contentsline {chapter}{\numberline {2}Wstęp}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Wprowadzenie}{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}Cel pracy}{5}{section.2.2}%
\contentsline {section}{\numberline {2.3}Motywacja}{6}{section.2.3}%
\contentsline {section}{\numberline {2.4}Zakres}{6}{section.2.4}%
\contentsline {chapter}{\numberline {3}Analiza projektowa}{7}{chapter.3}%
\contentsline {section}{\numberline {3.1}Analiza dostępnych rozwiązań}{7}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Konteneryzacja}{7}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Automatyzacja testów}{9}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Continuous Integration}{9}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Uruchamianie testów}{11}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Raporty}{11}{subsection.3.1.5}%
\contentsline {section}{\numberline {3.2}Szczegółowe założenia projektowe}{11}{section.3.2}%
\contentsline {chapter}{\numberline {4}Część praktyczna}{12}{chapter.4}%
\contentsline {section}{\numberline {4.1}Wykorzystane narzędzia}{12}{section.4.1}%
\contentsline {section}{\numberline {4.2}Projekt}{13}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Continuous Integration}{13}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Docker}{16}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Testy automatyczne}{20}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Raporty}{25}{subsection.4.2.4}%
\contentsline {subsection}{\numberline {4.2.5}Wydajność rozwiązania}{27}{subsection.4.2.5}%
\contentsline {chapter}{\numberline {5}Podsumowanie}{29}{chapter.5}%
\contentsline {section}{\numberline {5.1}Co udało się wykonać w ramach pracy?}{29}{section.5.1}%
\contentsline {section}{\numberline {5.2}Napotkane problemy}{29}{section.5.2}%
\contentsline {section}{\numberline {5.3}Możliwości rozwoju projektu}{29}{section.5.3}%
\contentsline {chapter}{Bibliografia}{31}{section*.9}%
\contentsline {chapter}{Spis rysunków}{33}{section*.11}%
\contentsline {chapter}{Spis listingów}{34}{section*.13}%
\contentsline {chapter}{Zawartość płyty CD}{35}{section*.15}%
\contentsline {chapter}{Oświadczenie o udostępnianiu pracy dyplomowej}{36}{Item.27}%
\contentsline {chapter}{Oświadczenie autorskie}{37}{Item.27}%
